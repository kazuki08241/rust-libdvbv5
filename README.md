# Rust-libdvbv5

GitLab CI: [![GitLab CI](https://gitlab.com/Russel/rust-libdvbv5/badges/master/pipeline.svg)](https://gitlab.com/Russel/rust_libdvbv5)
&nbsp;&nbsp;&nbsp;&nbsp;
Licence: [![Licence](https://img.shields.io/badge/license-LGPL_3-green.svg)](https://www.gnu.org/licenses/lgpl-3.0.en.html)

Rust support for using the
[libdvbv5 library](https://linuxtv.org/docs/libdvbv5/)
from the
[Video for Linux](https://www.linuxtv.org/wiki/index.php/Development:_Video4Linux_APIs)
project that is part of the
[Linux TV](https://www.linuxtv.org/) effort.

Depends on [dvbv5-sys](https://crates.io/crates/dvbv5-sys) which provides the Rust FFI to the libdvbv5 C API.

## Licence

This code is licenced under LGPLv3.
[![Licence](https://www.gnu.org/graphics/lgplv3-147x51.png)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
