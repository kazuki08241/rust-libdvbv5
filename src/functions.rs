/*
 *  libdvbv5 — a Rust binding to the libdvbv5 library from V4L2.
 *
 *  Copyright © 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Some functions wrapping dvbv5-sys functions so as to provide a more Rust oriented API.

use std::path::Path;

use dvbv5_sys;

use crate::enums::dtv_retrievable_properties;
use crate::types::{EntryPtr, FilePtr, FrontendParametersPtr, ScanHandlerPtr};

/// Add a newly found transponder to the list of transponder to be scanned.
pub fn add_scaned_transponders(
    frontend_parameters: &FrontendParametersPtr,
    scan_handler: &ScanHandlerPtr,
    file_ptr: &FilePtr,
    entry: &EntryPtr,
) {
    unsafe {
        dvbv5_sys::dvb_add_scaned_transponders(
            frontend_parameters.get_c_ptr(),
            scan_handler.get_c_ptr(),
            file_ptr.get_first_entry(),
            entry.get_c_ptr(),
        );
    }
}

/// Retrieve the property specified by `command` from the `entry`.
pub fn retrieve_entry_prop(
    entry: &EntryPtr,
    command: dtv_retrievable_properties,
) -> Result<u32, ()> {
    let mut value = 0;
    unsafe {
        match dvbv5_sys::dvb_retrieve_entry_prop(entry.get_c_ptr(), command as u32, &mut value) {
            0 => Ok(value),
            -1 => Err(()),
            _ => panic!("dvbv5_sys::dvb_retrieve_entry_prop returned a value other than 0 or -1."),
        }
    }
}

/// Add some channels to `channels_file`.
pub fn store_channel(
    mut channels_file: *mut dvbv5_sys::dvb_file,
    frontend_parameters: &FrontendParametersPtr,
    scan_handler: &ScanHandlerPtr,
    get_detected: bool,
    get_nit: bool,
) -> Result<*mut dvbv5_sys::dvb_file, ()> {
    // FilePtr, ()> {
    unsafe {
        match dvbv5_sys::dvb_store_channel(
            &mut channels_file,
            frontend_parameters.get_c_ptr(),
            scan_handler.get_c_ptr(),
            get_detected as i32,
            get_nit as i32,
        ) {
            0 => Ok(channels_file),
            -1 => Err(()),
            _ => panic!("dvbv5_sys::dvb_store_channel returned a value other than 0 or -1."),
        }
    }
}

/// Write a file in a given format.
pub fn write_file_format(
    path: &Path,
    file_ptr: &FilePtr,
    delsys: dvbv5_sys::fe_delivery_system,
    format: dvbv5_sys::dvb_file_formats,
) -> bool {
    unsafe {
        match dvbv5_sys::dvb_write_file_format(
            path.to_str().unwrap().as_ptr() as *const i8,
            file_ptr.get_c_ptr(),
            delsys as u32,
            format,
        ) {
            0 => true,
            -1 => false,
            _ => panic!("dvbv5_sys::dvb_write_file_format returned a value other than 0 or -1."),
        }
    }
}
