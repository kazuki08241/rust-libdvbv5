/*
 *  libdvbv5 — a Rust binding to the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! This crate provides some Rust facing abstractions over the C API of the
//! [libdvbv5 library](https://linuxtv.org/docs/libdvbv5/) that is part of the
//! [V4L2 project](https://linuxtv.org/wiki/index.php/V4l-utils) that is part of the
//! [Linux TV](https://linuxtv.org) effort.
//!
//! Linux has kernel level support for DVB devices. Working with them using the
//! system calls required is not easy. libdvbv5 provides an abstraction/middleware
//! layer over the kernel support to make working with DVB devices much easier.
//! The library is though focused on providing support for C programmers.
//!
//! The [dvbv5-sys crate](https://gitlab.com/Russel/rust-libdvbv5-sys) provides the Rust FFI
//! to the C API of libdvbv5. This crate provides various abstractions over the FFI to support
//! Rust programmers.

mod enums;
pub use enums::*;

mod functions;
pub use functions::*;

mod types;
pub use types::*;

// Rexport all the enums from dvbv5_sys.
pub use dvbv5_sys::{
    descriptors, dmx_buffer_flags, dmx_input, dmx_output, dmx_ts_pes, dvb_country_t,
    dvb_dev_change_type, dvb_dev_type, dvb_file_formats, dvb_mpeg_es_frame_t, dvb_quality,
    dvb_sat_polarization, dvb_streams, fe_bandwidth, fe_caps, fe_code_rate, fe_delivery_system,
    fe_guard_interval, fe_hierarchy, fe_interleaving, fe_modulation, fe_pilot, fe_rolloff,
    fe_sec_mini_cmd, fe_sec_tone_mode, fe_sec_voltage, fe_spectral_inversion, fe_status,
    fe_transmit_mode, fe_type, fecap_scale_params,
};
