/*
 *  libdvbv5 — a Rust binding to the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Various types to abstract away from the low level pointers of the C API.
//! Also add automated RAII features instead of  everything being manual.

use std::ffi::{CStr, CString};
use std::os::raw::c_int;
use std::path::Path;
use std::str;

use libc;

use dvbv5_sys;

/// The identifier for a frontend on a given adaptor.
#[derive(Clone, Debug, PartialEq)]
pub struct FrontendId {
    pub adapter_number: u8,
    pub frontend_number: u8,
}

/// A wrapper around a `*mut dvbv5_sys::dvb_v5_fe_parms` to provide automated RAII.
#[derive(PartialEq)]
pub struct FrontendParametersPtr {
    ptr: *mut dvbv5_sys::dvb_v5_fe_parms,
}

impl FrontendParametersPtr {
    /// Set up a frontend.
    ///
    /// * `fei` – a `FrontendId` identifying the frontend to be used.
    /// * `verbose` – an `Option` `u32`` specifying the level of verbosity. Default 0.
    /// * `use_legacy_call` – an `Option` `bool` with `true` meaning use DVBv3 instead of DVBv5. Default `false`.
    pub fn new(
        fei: &FrontendId,
        verbose: Option<u32>,
        use_legacy_call: Option<bool>,
    ) -> Result<FrontendParametersPtr, ()> {
        let verbose = verbose.unwrap_or(0);
        let use_legacy_call = use_legacy_call.unwrap_or(false);
        let ptr = unsafe {
            dvbv5_sys::dvb_fe_open(
                fei.adapter_number as i32,
                fei.frontend_number as i32,
                verbose,
                use_legacy_call as u32,
            )
        };
        if ptr.is_null() {
            Err(())
        } else {
            Ok(FrontendParametersPtr { ptr })
        }
    }

    /// Log a message using the log function for this frontend.
    pub fn log(&self, level: crate::log_level, message: &str) {
        unsafe {
            // Pointer access and call of logger are unsafe.
            match (*self.ptr).logfunc {
                Some(logger) => logger(level as i32, CString::new(message).unwrap().as_ptr()),
                None => panic!("XXXX no logger defined XXXX: {}", message),
            }
        }
    }

    pub fn get_current_sys(&self) -> dvbv5_sys::fe_delivery_system {
        unsafe { (*self.ptr).current_sys }
    }

    pub fn get_abort(&self) -> bool {
        unsafe { (*self.ptr).abort != 0 }
    }

    #[doc(hidden)]
    pub(crate) fn get_c_ptr(&self) -> *mut dvbv5_sys::dvb_v5_fe_parms {
        self.ptr
    }
}

impl Drop for FrontendParametersPtr {
    fn drop(&mut self) {
        unsafe {
            dvbv5_sys::dvb_fe_close(self.ptr);
        }
    }
}

impl std::fmt::Debug for FrontendParametersPtr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "FrontendParametersPtr{{{:?}}}", unsafe { *self.ptr })
    }
}

/// A wrapper around a `*mut dvbvb_sys::dvb_entry` to provide Rust-level abstraction.
#[derive(PartialEq)]
pub struct EntryPtr {
    ptr: *mut dvbv5_sys::dvb_entry,
}

impl EntryPtr {
    /// Wrap a `*mut dvbv5_sys::dvb_entry` as an `EntryPtr`.
    pub fn new_from_dvb_entry_ptr(ptr: *mut dvbv5_sys::dvb_entry) -> Result<Self, ()> {
        if ptr.is_null() {
            Err(())
        } else {
            Ok(Self { ptr })
        }
    }

    /// Get the channel string from the entry.
    pub fn get_channel(&self) -> Result<&str, String> {
        unsafe {
            if (*self.ptr).channel.is_null() {
                Err("channel pointer was null.".to_string())
            } else {
                match CStr::from_ptr((*self.ptr).channel).to_str() {
                    Ok(s) => Ok(s),
                    Err(e) => Err(e.to_string()),
                }
            }
        }
    }

    /// Get the vchannel from the entry.
    pub fn get_vchannel(&self) -> Result<&str, String> {
        unsafe {
            if (*self.ptr).vchannel.is_null() {
                Err("vchannel pointer was null.".to_string())
            } else {
                match CStr::from_ptr((*self.ptr).vchannel).to_str() {
                    Ok(s) => Ok(s),
                    Err(e) => Err(e.to_string()),
                }
            }
        }
    }

    /// Get the location from the entry.
    pub fn get_location(&self) -> Result<&str, String> {
        unsafe {
            if (*self.ptr).location.is_null() {
                Err("location pointer was null.".to_string())
            } else {
                match CStr::from_ptr((*self.ptr).location).to_str() {
                    Ok(s) => Ok(s),
                    Err(e) => Err(e.to_string()),
                }
            }
        }
    }

    #[doc(hidden)]
    pub(crate) fn get_c_ptr(&self) -> *mut dvbv5_sys::dvb_entry {
        self.ptr
    }
}

impl std::fmt::Debug for EntryPtr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "EntryPtr{{{:?}}}", unsafe { *self.ptr })
    }
}

/// A wrapper around a `*mut dvbv5_sys::dvb_file` in order to provide automated RAII.
#[derive(PartialEq)]
pub struct FilePtr {
    ptr: *mut dvbv5_sys::dvb_file,
}

impl FilePtr {
    /// Open a `Path` as  `FilePtr`
    ///
    /// * `path` – the `Path` to the file to open.
    /// * `delsys` – an `Option` `dvbv5_sys::fe_delivery_system` specifying the delivery system.
    /// Default `dvbv5_sys::fe_delivery_system_SYS_UNDEFINED`
    /// * `format` – an `Option` `dvbv5_sys::dvb_file_formats` specifying the format of the file being opened.
    /// Default `dvbv5_sys::dvb_file_formats_FILE_DVBV5`
    pub fn new(
        path: &Path,
        delsys: Option<dvbv5_sys::fe_delivery_system>,
        format: Option<dvbv5_sys::dvb_file_formats>,
    ) -> Result<Self, ()> {
        let delsys = delsys.unwrap_or(dvbv5_sys::fe_delivery_system::SYS_UNDEFINED);
        let format = format.unwrap_or(dvbv5_sys::dvb_file_formats::FILE_DVBV5);
        let ptr = unsafe {
            dvbv5_sys::dvb_read_file_format(
                CString::new(path.to_str().unwrap()).unwrap().as_ptr(),
                delsys as u32,
                format,
            )
        };
        Self::new_from_dvb_file_ptr(ptr)
    }

    /// Wrap a `*mut dvbv5_sys::dvb_file` as a `FilePtr`.
    pub fn new_from_dvb_file_ptr(ptr: *mut dvbv5_sys::dvb_file) -> Result<Self, ()> {
        if ptr.is_null() {
            Err(())
        } else {
            Ok(Self { ptr })
        }
    }

    /// Return an iterator over the `dvbv5_sys::dvb_entries` in this `FilePtr`.
    pub fn iter(&self) -> FilePtrIter {
        FilePtrIter {
            current: self.get_first_entry(),
        }
    }

    #[doc(hidden)]
    pub(crate) fn get_first_entry(&self) -> *mut dvbv5_sys::dvb_entry {
        unsafe { (*self.ptr).first_entry }
    }

    #[doc(hidden)]
    pub(crate) fn get_c_ptr(&self) -> *mut dvbv5_sys::dvb_file {
        self.ptr
    }
}

impl Drop for FilePtr {
    fn drop(&mut self) {
        dvbv5_sys::dvb_file_free(self.ptr);
    }
}

impl std::fmt::Debug for FilePtr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "FilePtr{{{:?}}}", unsafe { *self.ptr })
    }
}

/// Struct for implementing an iterator over the `EntryPtr`s of a `FilePtr`.
#[derive(PartialEq)]
pub struct FilePtrIter {
    current: *mut dvbv5_sys::dvb_entry,
}

impl Iterator for FilePtrIter {
    type Item = EntryPtr;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_null() {
            None
        } else {
            let rv = self.current;
            self.current = unsafe { (*self.current).next };
            Some(EntryPtr::new_from_dvb_entry_ptr(rv).unwrap())
        }
    }
}

/// A wrapper around a `c_int` that is the file descriptor number of the demux device
/// for a given frontend. The wrapper enforces automated RAII.
#[derive(PartialEq)]
pub struct DmxFd {
    fd: c_int,
}

impl DmxFd {
    /// Open the demux device of a given frontend.
    pub fn new(fei: &FrontendId) -> Result<Self, ()> {
        let fd = unsafe {
            dvbv5_sys::dvb_dmx_open(fei.adapter_number as i32, fei.frontend_number as i32)
        };
        if fd < 0 {
            Err(())
        } else {
            Ok(Self { fd })
        }
    }
}

impl Drop for DmxFd {
    fn drop(&mut self) {
        unsafe {
            dvbv5_sys::dvb_dmx_close(self.fd);
        }
    }
}

impl std::fmt::Debug for DmxFd {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "DmxFd{{fd: {}}}", self.fd)
    }
}

/// A wrapper around a `*mut dvbv5_sys::dvb_v5_descriptors` providing automated RAII and
/// enabling easy scanning for channels on the channels/transponders of a given transmitter
/// using a DVB device.
#[derive(PartialEq)]
pub struct ScanHandlerPtr {
    ptr: *mut dvbv5_sys::dvb_v5_descriptors,
}

impl ScanHandlerPtr {
    /// Perform a scan on an opened frontend.
    ///
    /// * `frontend_parameters` – a `FrontendParametersPtr` instance giving details of
    /// the frontend.
    /// * `entry` – DVB file entry that corresponds to a transponder to be tuned.
    /// *A return value parameter.*
    /// * `dmx_fd` – an opened demux file descriptor.
    /// * `check_frontend` – an `Option` pointer to a function (C linkage) that will show the
    /// frontend status while tuning into a transponder.
    /// * `other_nit` – an `Option` `bool`; use alternate table IDs for NIT and other tables. Default `false`.
    /// * `timeout_multiplier` – an `Option` unsigned integer; increases the timeout for each table reception. Default 1.
    pub fn new(
        frontend_parameters: &FrontendParametersPtr,
        entry: &EntryPtr,
        dmx_fd: &DmxFd,
        check_frontend: dvbv5_sys::check_frontend_t,
        other_nit: Option<bool>,
        timeout_multiplier: Option<u32>,
    ) -> Result<Self, ()> {
        let other_nit = other_nit.unwrap_or(false);
        let timeout_multiplier = timeout_multiplier.unwrap_or(1);
        let ptr = unsafe {
            dvbv5_sys::dvb_scan_transponder(
                frontend_parameters.ptr,
                entry.get_c_ptr(),
                dmx_fd.fd,
                check_frontend,
                std::ptr::null_mut::<libc::c_void>(),
                other_nit as u32,
                timeout_multiplier,
            )
        };
        if ptr.is_null() {
            Err(())
        } else {
            Ok(Self { ptr })
        }
    }

    #[doc(hidden)]
    pub(crate) fn get_c_ptr(&self) -> *mut dvbv5_sys::dvb_v5_descriptors {
        self.ptr
    }
}

impl Drop for ScanHandlerPtr {
    fn drop(&mut self) {
        unsafe {
            dvbv5_sys::dvb_scan_free_handler_table(self.ptr);
        }
    }
}

impl std::fmt::Debug for ScanHandlerPtr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "ScanHandlerPtr{{{:?}}}", unsafe { *self.ptr })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn frontend_id_equality() {
        let a = FrontendId {
            adapter_number: 1,
            frontend_number: 2,
        };
        let b = FrontendId {
            adapter_number: 1,
            frontend_number: 1,
        };
        let c = FrontendId {
            adapter_number: 0,
            frontend_number: 2,
        };
        let d = FrontendId {
            adapter_number: 0,
            frontend_number: 0,
        };
        assert_eq!(a, a);
        assert_ne!(a, b);
        assert_ne!(a, c);
        assert_ne!(a, d);

        assert_ne!(b, a);
        assert_eq!(b, b);
        assert_ne!(b, c);
        assert_ne!(b, d);

        assert_ne!(c, a);
        assert_eq!(c, c);
        assert_ne!(c, b);
        assert_ne!(c, d);

        assert_ne!(d, a);
        assert_ne!(d, b);
        assert_ne!(d, c);
        assert_eq!(d, d);
    }

    #[test]
    fn frontend_id_clone() {
        let a = FrontendId {
            adapter_number: 1,
            frontend_number: 2,
        };
        let b = a.clone();
        assert_eq!(a, b);
    }

    #[test]
    fn format_frontend_id() {
        assert_eq!(
            format!(
                "{:?}",
                FrontendId {
                    adapter_number: 1,
                    frontend_number: 2
                }
            ),
            "FrontendId { adapter_number: 1, frontend_number: 2 }"
        );
    }

    #[test]
    fn format_frontend_parameters_ptr() {
        match FrontendParametersPtr::new(
            &FrontendId {
                adapter_number: 0,
                frontend_number: 0,
            },
            None,
            None,
        ) {
            Ok(fp) => {
                let result = format!("{:?}", fp);
                //assert_eq!(result, "");
                assert!(result.starts_with("FrontendParametersPtr{dvb_v5_fe_parms{"));
                assert!(result.ends_with("}"));
            }
            Err(_) => println!("Could not get a frontend, no test undertaken."),
        };
    }

    #[test]
    fn format_file_ptr() {
        let the_file =
            FilePtr::new_from_dvb_file_ptr(Box::into_raw(Box::new(dvbv5_sys::dvb_file {
                fname: 0 as *mut i8,
                n_entries: 0,
                first_entry: 0 as *mut dvbv5_sys::dvb_entry,
            })))
            .unwrap();
        assert_eq!(
            format!("{:?}", the_file),
            "FilePtr{dvb_file { fname: 0x0, n_entries: 0, first_entry: 0x0 }}"
        );
    }

    fn create_dvb_entry_on_heap() -> *mut dvbv5_sys::dvb_entry {
        let item = Box::new(dvbv5_sys::dvb_entry {
            props: [dvbv5_sys::dtv_property {
                cmd: 0,
                reserved: [0; 3],
                u: dvbv5_sys::dtv_property__bindgen_ty_1 { data: 0 },
                result: 0,
            }; 70],
            n_props: 0,
            next: 0 as *mut dvbv5_sys::dvb_entry,
            service_id: 0,
            video_pid: 0 as *mut u16,
            audio_pid: 0 as *mut u16,
            other_el_pid: 0 as *mut dvbv5_sys::dvb_elementary_pid,
            video_pid_len: 0,
            audio_pid_len: 0,
            other_el_pid_len: 0,
            channel: 0 as *mut i8,
            vchannel: 0 as *mut i8,
            location: 0 as *mut i8,
            sat_number: 0,
            freq_bpf: 0,
            diseqc_wait: 0,
            lnb: 0 as *mut i8,
            network_id: 0,
            transport_id: 0,
        });
        Box::into_raw(item)
    }

    fn create_dvb_file_on_heap() -> *mut dvbv5_sys::dvb_file {
        let item = Box::new(dvbv5_sys::dvb_file {
            fname: 0 as *mut i8,
            n_entries: 2,
            first_entry: 0 as *mut dvbv5_sys::dvb_entry,
        });
        Box::into_raw(item)
    }

    #[test]
    fn iterate_over_file_ptr() {
        let item_a = create_dvb_entry_on_heap();
        let item_b = create_dvb_entry_on_heap();
        let item_c = create_dvb_entry_on_heap();
        unsafe {
            (*item_a).next = item_b;
        }
        unsafe {
            (*item_b).next = item_c;
        }
        let the_file = create_dvb_file_on_heap();
        unsafe {
            (*the_file).first_entry = item_a;
            (*the_file).n_entries = 3;
        }
        let entry_a = EntryPtr::new_from_dvb_entry_ptr(item_a).unwrap();
        let entry_b = EntryPtr::new_from_dvb_entry_ptr(item_b).unwrap();
        let entry_c = EntryPtr::new_from_dvb_entry_ptr(item_c).unwrap();
        let file_ptr = FilePtr::new_from_dvb_file_ptr(the_file).unwrap();
        let mut iter = file_ptr.iter();
        assert_eq!(iter.next().unwrap(), entry_a);
        assert_eq!(iter.next().unwrap(), entry_b);
        assert_eq!(iter.next().unwrap(), entry_c);
        assert_eq!(iter.next(), None);
        for (index, item_ptr) in file_ptr.iter().enumerate() {
            match index {
                0 => assert_eq!(item_ptr, entry_a),
                1 => assert_eq!(item_ptr, entry_b),
                2 => assert_eq!(item_ptr, entry_c),
                _ => assert!(false, "Got more than three items."),
            }
        }
    }

    #[test]
    fn format_dmx_fd() {
        let datum = DmxFd { fd: 17 };
        assert_eq!(format!("{:?}", datum), "DmxFd{fd: 17}");
    }

    // TODO Think how to test the formatting of a ScanHandlerPtr
}
